package be.kdg.pro3;

import java.util.Arrays;
import java.util.Comparator;

public class Main {
	public static void main(String[] args) {
		String[] jokes = {
			"Chuck Norris",
			"Mustard and yesterday",
			"The fool on the hill",
			"Invisible man"};
		// Sorting using MyComparator
		//Arrays.sort(jokes,new MyComparator());

		// sorting using a lambda
//		Arrays.sort(jokes,(String o1, String o2) -> {
//			return o1.length() - o2.length();
//		});

		// lambdas can even be shorter
		//Arrays.sort(jokes,( o1,  o2) -> o1.length() - o2.length());
		System.out.println(Arrays.toString(jokes));

		// Generating the comparator
		Arrays.sort(jokes,Comparator.comparing(o -> o.length()));

		// using a method reference
		Arrays.sort(jokes,Comparator.comparing(String::length));

	}
}

class MyComparator implements Comparator<String>{

	@Override
	public int compare(String o1, String o2) {
		return o1.length() - o2.length();
	}
}